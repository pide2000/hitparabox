from setuptools import setup

setup(name='hitparabox',
      version='0.1',
      description='Show actual played song of a webradio ',
      url='http://gitlab.com/pide2000/hitparabox',
      author='Peter Bolch',
      author_email='peter+dev@bolch.space',
      license='MIT',
      packages=['hitparabox'],
      zip_safe=False)