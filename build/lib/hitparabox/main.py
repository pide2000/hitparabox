import argparse

from hitparabox import Hitparabox

parser = argparse.ArgumentParser()
parser.add_argument('--inkyPhat', '-i', required=False, action='store_true', help="Use Inky Phat to show")
parser.add_argument('--station', '-s', type=str, required=True, help="Name of the radio station")
parser.add_argument('--songListUrl', '-l', type=str, required=True, help="Url to get songlist from")
args = parser.parse_args()

station = args.station
song_list_url = args.songListUrl

if args.inkyPhat:
    import json
    from hitparabox.inky_phat_writer import inkyPhatWriter

    inky_phat_writer = inkyPhatWriter()

    def song_list_getter(song_list):
        song_list_json = json.loads(song_list)
        inky_phat_writer.print_song_list(song_list_json, station)

    def stop_function():
        return True

    hitparabox = Hitparabox()
    hitparabox.get_song_list_stream(song_list_getter, stop_function)

else:
    import urllib
    import Figlet

    f = Figlet(font='slant')
    print(f.renderText(station))

    search_base_url = "http://de.wikipedia.org/w/index.php/?search="

    def render(data):
        dat = json.loads(data)
        for obj in dat:
            band = obj.get('artist')
            title = obj.get('title')
            artist_wiki = f"{search_base_url}{urllib.parse.quote(band)}"
            title_wiki = f"{search_base_url}{urllib.parse.quote(title)}"
            print(f"{band} -- {title} ::::::: {artist_wiki} ::: {title_wiki}")
        print("--------------------")

    def song_list_getter(song_list):
        render(song_list)

    def stop_function():
        return True

    hitparabox = Hitparabox()

    hitparabox.get_song_list_stream(song_list_getter, stop_function)
